# jetbot ros2 docker [ros:humble]

## requirements
### install ina219
```bash
git clone git@gitlab.com:telelian-robot/utils/load-ina219.git
cd load-ina219
sudo ./install.sh
```

## build
```bash
docker build -t ros_humble_jetbot_build . -f Dockerfile.build
```
## run_docker.sh
```bash
#!/bin/bash
CONTAINER_NAME=ros_humble
export DISPLAY=:0
xhost +local:docker

# Re-use existing container.
if [ "$(docker ps -a --quiet --filter status=running --filter name=$CONTAINER_NAME)" ]; then
    docker exec -i -t -u admin --workdir /workspaces $CONTAINER_NAME /bin/bash $@
    exit 0
fi

docker run -it --rm --network host \
--privileged \
-v /dev/*:/dev/* \
-v /etc/localtime:/etc/localtime:ro \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-v $HOME/.Xauthority:/home/admin/.Xauthority:rw \
-e DISPLAY=:0 \
-e NVIDIA_VISIBLE_DEVICES=all \
-e NVIDIA_DRIVER_CAPABILITIES=all \
-e RMW_IMPLEMENTATION=rmw_cyclonedds_cpp \
-v /run/jtop.sock:/run/jtop.sock:ro \
-v /usr/bin/tegrastats:/usr/bin/tegrastats \
-v /tmp/argus_socket:/tmp/argus_socket \
-v /usr/local/cuda-11.4/targets/aarch64-linux/lib/libcusolver.so.11:/usr/local/cuda-11.4/targets/aarch64-linux/lib/libcusolver.so.11 \
-v /usr/local/cuda-11.4/targets/aarch64-linux/lib/libcusparse.so.11:/usr/local/cuda-11.4/targets/aarch64-linux/lib/libcusparse.so.11 \
-v /usr/local/cuda-11.4/targets/aarch64-linux/lib/libcurand.so.10:/usr/local/cuda-11.4/targets/aarch64-linux/lib/libcurand.so.10 \
-v /usr/local/cuda-11.4/targets/aarch64-linux/lib/libnvToolsExt.so:/usr/local/cuda-11.4/targets/aarch64-linux/lib/libnvToolsExt.so \
-v /usr/local/cuda-11.4/targets/aarch64-linux/lib/libcupti.so.11.4:/usr/local/cuda-11.4/targets/aarch64-linux/lib/libcupti.so.11.4 \
-v /usr/local/cuda-11.4/targets/aarch64-linux/lib/libcudla.so.1:/usr/local/cuda-11.4/targets/aarch64-linux/lib/libcudla.so.1 \
-v /usr/local/cuda-11.4/targets/aarch64-linux/include/nvToolsExt.h:/usr/local/cuda-11.4/targets/aarch64-linux/include/nvToolsExt.h \
-v /usr/lib/aarch64-linux-gnu/tegra:/usr/lib/aarch64-linux-gnu/tegra \
-v /usr/src/jetson_multimedia_api:/usr/src/jetson_multimedia_api \
-v /opt/nvidia/nsight-systems-cli:/opt/nvidia/nsight-systems-cli \
--pid=host \
-v /opt/nvidia/vpi2:/opt/nvidia/vpi2 \
-v /usr/share/vpi2:/usr/share/vpi2 \
-v /run/jtop.sock:/run/jtop.sock:ro \
-v $HOME/workspaces:/workspaces \
--workdir /workspaces \
--user="admin" \
--name $CONTAINER_NAME \
ros_humble_jetbot_build:latest \
/bin/bash
```

### run teleop keyboard in ros humble docker
```bash
ros2 run teleop_twist_keyboard teleop_twist_keyboard --ros-args -r __ns:=/jetbot_ros_ai
```

## joystick
### build
```bash
docker build -t ros_humble_joystick . -f Dockerfile.joystick
```

### run
```bash
docker run -it --rm --network host \
--privileged \
-e FASTRTPS_DEFAULT_PROFILES_FILE=/usr/local/share/middleware_profiles/rtps_udp_profile.xml \
--workdir /workspaces \
--user="admin" \
--name joystick \
ros_humble_joystick:latest
```

## jetbot
### build
```bash
docker build -t ros_humble_jetbot . -f Dockerfile.jetbot
```

### run
```bash
docker run -it --rm --network host \
--privileged \
-e FASTRTPS_DEFAULT_PROFILES_FILE=/usr/local/share/middleware_profiles/rtps_udp_profile.xml \
-v $HOME/workspaces:/workspaces \
--workdir /workspaces \
--user="admin" \
--name jetbot \
ros_humble_jetbot:latest /bin/bash
```
